syntax on
set expandtab
set tabstop=4
set ai
set sw=4
set smartcase
set ignorecase
hi Comment ctermfg=lightblue
set encoding=utf8
set pastetoggle=<F2>
set modeline
map <F3> :set wrap!<cr>
set title
set titleold=
set history=1000
set mouse=

"set foldmethod=syntax "syntax highlighting items specify folds
"set foldcolumn=1 "defines 1 col at window left, to indicate folding
"let javaScript_fold=1 "activate folding by JS syntax
"set foldlevelstart=99 "start file with all folds opened
