# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

source_exist() {
  if [ -f $1 ]; then
    . $1
  fi
}

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
export HISTCONTROL=ignoredups
export HISTSIZE=5000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize
shopt -s histappend

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"
# Let less use utf8 charset
export LESSCHARSET=utf-8

# set variable identifying the chroot you work in (used in the prompt below)
#if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
#    debian_chroot=$(cat /etc/debian_chroot)
#fi

# set a fancy prompt (non-color, unless we know we "want" color)
#case "$TERM" in
#xterm-color)
#    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#    ;;
#*)
#    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#    ;;
#esac

# Comment in the above and uncomment this below for a color prompt
#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
#    ;;
#*)
#    ;;
#esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

source_exist ~/.bash_aliases
source_exist ~/.bash_prompt
source_exist ~/.bash_add_keys
source_exist /etc/bash_completion
source_exist ~/.bash_completion
source_exist ~/.bashmarks
source_exist ~/.bash_extra

# set PATH so it includes user's private bin if it exists
if [ -d ~/bin ] ; then
    PATH="${PATH}":~/bin
fi
if [ -d ~/scripts ] ; then
    PATH="${PATH}":~/scripts
fi

# Set Subversion editor
export SVN_EDITOR=vim
export EDITOR=vim
export VISUAL=vim

# Python setup
export PYTHONPATH=~/pythonlib
export PYTHONDONTWRITEBYTECODE=1

[ -d ~/envsecret ] && . ~/envsecret/envsecret setup

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

[ "$HOSTNAME" == "superstats" ] && {
  export wwwp="/var/www/superstats.dk"
  export wwwt="/var/www/test"
}

source_exist ~/.cargo/env

function error() {
  echo $@ 1>&2
  exit 1
}
# Print red/green text to terminal
function ok()   { tput setaf 2; tput bold; printf "${1:-OK\n}"; tput sgr0; }
function fail() { tput setaf 1; tput bold; printf "${1:-FAIL\n}"; tput sgr0; }
function warn() { tput setaf 3; tput bold; printf "${1:-WARNING\n}"; tput sgr0; }
export -f error
export -f ok
export -f fail
export -f warn


# These colors are used in update_prompt command and interactively
export RESET="$(tput sgr0)"
export GREEN="$(tput setaf 2)"
export RED="$(tput setaf 1; tput bold)"
export WHITE="$(tput setaf 7; tput bold)"
export YELLOW="$(tput setaf 3; tput bold)"
export BLUE="$(tput setaf 4; tput bold)"
export LPURPLE="$(tput setaf 5; tput bold)"
export LGREEN="$(tput setaf 2; tput bold)"

