case "$HOSTNAME" in
  romkugle)
    TUNNEL_SERVER=bordfodboldbordet.dk
    TUNNEL_PORTWEB=8080
    TUNNEL_PORTSSH=2220
    ;;
  rainbow)
    TUNNEL_SERVER=bordfodboldbordet.dk
    TUNNEL_PORTWEB=8081
    TUNNEL_PORTSSH=2221
    ;;
  pi2)
    TUNNEL_SERVER=bordfodboldbordet.dk
    TUNNEL_PORTWEB=8082
    TUNNEL_PORTSSH=2222
    ;;
  *)
    TUNNEL_SERVER=bordfodboldbordet.dk
    TUNNEL_PORTWEB=8088
    TUNNEL_PORTSSH=2228
esac

export TUNNEL_SERVER=$TUNNEL_SERVER
export TUNNEL_PORTSSH=$TUNNEL_PORTSSH
export TUNNEL_PORTWEB=$TUNNEL_PORTWEB

