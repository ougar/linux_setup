# Make sh lines which creates symlinks in ~ to the setup files in repo

# Function to check everything for a single file
# Called with a repo name and the corresponding name in ~
#
# 1: Check if repo file exists - if not, do nothing
# 2: Check if target is a dead link
# 3: Check if target is a file and is NOT a link - If yes, then give rm command
# 4: Check if target is a dir and is NOT a link - If yes, then ask user to clean it up
# 5: Check if file is not a link - If yes, then give ln -s command
#
# These commands can then be copy/pasted to make the links
# Not executed automatically, so I don't do something stupid

cd $(dirname $0)

notice() {
  echo $* >&2
}

create_link() {
  repo=$1
  active=$2
  # Test if repo file exists
  if [ ! -e ./$repo ]; then notice " - No such file in repo: $repo"; return; fi
  # Test if file does not exist but link exists (dead link)
  if [ ! -e ~/$active ]&&[ -h ~/$active ]; then notice " - Removing dead link $active"; rm ~/$active; fi
  # Test if file exists and NOT a link
  if [ -f ~/$active ]&&[ ! -h ~/$active ]; then notice " - Removing existing file ~/$active"; rm ~/$active; fi
  # Test if it is a directory and NOT a link
  if [ -d ~/$active ]&&[ ! -h ~/$active ]; then notice " - ~/$active is a regular directory"; return; fi
  # Test if everything already looks correct
  if [ -h ~/$active ]; then notice " - ~/$active exists and is already a link"; return; fi
  
  notice "Making symlink ~/$active"; 
  ln -s ~/linux_setup/$repo ~/$active;
}


# Run through home-dir conf files in repo and add to homedir with leading dot
for n in bash* vimrc gitconfig; do
  create_link $n .$n
done
create_link scripts scripts

