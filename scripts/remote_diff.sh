#!/bin/bash
####################################################
# Make a diff of one local file and one remote file.
####################################################

tmpfile=/tmp/.remotediff_tempfile$$

if [ $# -ne 2 ]; then
  printf "Wrong number of arguments. Two files should be specified.\n"
  exit 1
fi

if [ ! -f $1 ]; then
  printf "First argument must be a local file\n"
  exit 1
fi

if [ -z $(echo $2|grep :) ]; then
  printf "Second argument must be a remote file\n"
  exit 1
fi

if [ -z $(echo $2|cut -d: -f2) ]; then
  
  if [ "$(dirname $1)" = "." ]; then
    $(scp $2$PWD/$1 $tmpfile)
  else
    $(scp $2$1 $tmpfile)
  fi
else
  $(scp $2 $tmpfile)
fi  

diff $1 $tmpfile
rm -f $tmpfile
