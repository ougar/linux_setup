#!/bin/bash

printf "$1 [y/N] "
read ans
if [ "$ans" == "y" ]; then
  exit 0;
else
  exit 1;
fi
