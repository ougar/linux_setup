#!/bin/bash
#############################################
# Secure copy a local file to a remote host,
# AND place the file in the same dir as the
# local file.
#############################################
if [ "$1" = "-r" ]; then
  r="-r"
  shift
fi

if [ $(echo $1|cut -b 1) = "/" ]; then
  destpath=$(dirname $1)
else
  destpath=$PWD/$(dirname $1)
fi

lastarg=`eval echo \"\$\{$#\}\"`
filearg=`echo $*|sed s/$lastarg//`

if [ ! $(echo $lastarg|cut -b ${#lastarg}) = ":" ]; then
  comarg="$lastarg:$destpath"
else
  comarg="$lastarg$destpath"
fi
if [ $# -eq 2 ]; then
  echo file: $1 -\> $comarg
else
  echo multiple files -\> $comarg
fi
/usr/bin/scp $r $filearg $comarg
