#!/bin/bash

[ -f $HOME/.agent ] || exit 1

. $HOME/.agent

ps -p $SSH_AGENT_PID &> /dev/null || exit 2

ls $SSH_AUTH_SOCK &> /dev/null || exit 3

exit 0

