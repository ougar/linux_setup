#!/bin/bash

MAIL=/home/vmail/ougar.dk/$USER

# ssh-aliases
COMPUTERS="romkugle rainbow ougar"

for comp in $COMPUTERS; do	# Add an alias for all the above computers
  alias $comp="ssh $comp"	# so I can ssh to them by only typing the
done				# computer name.

# Standard aliases
alias cp='cp -i'
alias mv='mv -i'
#alias ls='ls --color'
alias ll='ls -l'
alias l='ll'
alias llt='ll --sort=time -r'
alias lslatest='ls --sort=time --color=none|head -1'
alias lsn='ls --color=none'
alias lln='ll --color=none'

# disk usage aliases
alias du0='du --max-depth=0 -x'
alias du1='du --max-depth=1 -x'
alias du2='du --max-depth=2 -x'
alias dus='du1|sort -n'

# Git
alias diffgit='diff -r --exclude=".git"'
alias gits="git status"
alias gitl="git logg|head"

# Smart shortcuts
alias igrep='egrep -i'

# CVS aliases
alias status='cvs status 2>&1 |egrep "File|Examining"'
alias status2='cvs status 2>&1 |egrep File|grep -v Up-to-date'

# Run programs from home

# Other aliases
alias xfig='xfig -metric -startgridmode 3 -startposnmode 4 -zoom .95'

alias mmm='nfrm /var/spool/mail/kristian'
alias phplib='cd /home/www/phplib'

alias gotest='cd /var/www/test'
alias goprod='cd /var/www/superstats.dk'

alias puttytitle='. ~/scripts/settitle.sh'
alias startagent='ssh-agent|head -2>~/.agent && . ~/.agent && ssh-add'
alias ss="ssh superstats"
alias envx="envsecret export"
alias envxs="envsecret -s export"
alias gits="git status"
alias gitl="git logg|head"

alias stamp="date +%Y%m%d-%H%M%S"

alias mysql=mariadb
alias mysqldump=mariadb-dump

# Make an alias for all www-sites in /home/www
[ -d /home/www ] &&
  for d in $(ls /home/www); do if [ -d /home/www/$d/html ]; then suf="/html"; else suf=""; fi; eval "alias www_${d%%.*}='cd /home/www/$d$suf'"; done

[ "$HOSTNAME" == "superstats" ] && {
  alias testerror='less /var/www/logs/test_error.log'
  alias testaccess='less /var/www/logs/test_access.log'
  alias proderror='less /var/www/logs/superstats.dk_error.log'
  alias prodaccess='less /var/www/logs/superstats.dk_access.log'
}

alias ve='find -maxdepth 3 -wholename "./*/bin/activate"'
alias vv='if [ "$HOME" == "$(pwd)" ]; then echo "You are not in a project directory"; else 
  ve|wc -l|grep -q "^1$" && { 
    if [ -n "$VIRTUAL_ENV" ]; then
      if [ "$(realpath $(ve))" == "$VIRTUAL_ENV/bin/activate" ]; then
        echo "This virtual env is allready activated"
        false
      else
        echo "Another virtual env ($VIRTUAL_ENV) is allready activated"
        false
      fi
    else
      echo "Activating $(ve)"
      . $(ve)
      true
    fi
  } || { echo "No unique venv found"; ve; false; }; fi'
alias newpy='[ -d venv ] && echo "venv allready exists" >&2 || { echo Creating virtual environment; python3 -m venv venv; . venv/bin/activate; pip install --upgrade pip | grep "Successfully installed"; }'

function needpass() {
  local varname
  varname=$1
  [ -n "$varname" ] || { echo "You need to specify name of secret"; return 1; }
  [ -n "${!varname}" ] || { echo "Secret '$varname' needed"; . $(readlink -f $(which envsecret)) export $varname || return 1; }
}
export -f needpass

alias myinflux='needpass INFLUXDB || { echo "Cannot connect to influxDB without credentials"; false; } && influx -username kristian -password $INFLUXDB'

function needvar() {
  local msg varname
  varname=$1
  shift
  msg=$*
  msg=${msg:-Env variable \'$varname\' needed}
  [ -n "${!varname}" ] || { echo "$msg"; [ $SHLVL -ne 1 ] && exit 1 || echo "Not quitting level 1 session"; false; }
}
export -f needvar


#function checkvar {
#
#if [ "$1" == "" ]; then
#  echo Error using checkvar: Usage: checkvar variable message exitcode
#  echo Variable must contain data or the message is printed and the program
#  echo exists with a value of exitcode. This means that if there are two
#  echo arguments the variable was empty.
#fi
#
#if [ "$3" == "" ]; then # Less than three arguments.  Var was empty
#  echo $1
#  exit $2
#fi
#}
